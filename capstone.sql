SELECT customerName FROM customers WHERE country IN ("Philippines")

SELECT contactLastName, contactFirstName FROM customers WHERE customerName IN ("La Rochelle Gifts");

SELECT productName, MSRP  FROM products WHERE productName IN ("The Titanic");

SELECT firstName, lastName FROM employees WHERE email IN ("jfirrelli@classicmodelcars.com");

SELECT customerName FROM customers WHERE state IS NULL; 

SELECT firstName, lastName, email FROM employees WHERE lastName IN ("Patterson") AND firstName IN ("Steve");

SELECT customerName, country, creditLimit FROM customers WHERE country != ("USA") AND creditLimit > 3000;

SELECT orderNumber FROM orders WHERE comments LIKE "%DHL%";

SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

SELECT DISTINCT country FROM customers; 

SELECT DISTINCT status FROM orders;

SELECT customerName, country FROM customers WHERE country IN ("USA", "France", "Canada");

SELECT firstName, lastName, city FROM employees
	JOIN offices ON employees.employeeNumber = offices.officeCode
	WHERE city IN ("Tokyo");

SELECT customerName FROM customers
	JOIN employees ON customers.salesRepEmployeeNumber = employees.reportsTo
	WHERE firstName In ("Thompson");

SELECT productName, customerName FROM products
	JOIN customers ON products.productLine = customers.salesRepEmployeeNumber;